package com.emb.ci.ejb;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bu
 */
public class CiEJBTest {

    private static EJBContainer container;

    public CiEJBTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        Map<String, Object> properties = new HashMap<>();
        properties.put(EJBContainer.MODULES, new File("target/classes"));
        container = EJBContainer.createEJBContainer(properties);
        System.out.println("Opening the container");
    }

    @AfterClass
    public static void tearDownClass() {
        container.close();
        System.out.println("Closing the container");
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getGreeting method, of class CiEJB.
     */
    @Test
    public void testGetGreeting() throws Exception {
        System.out.println("getGreeting");
        CiEJB instance = (CiEJB) container.getContext().lookup("java:global/classes/CiEJB");
        String expResult = "Hello";
        String result = instance.doTest();
        assertEquals(expResult, result);
    }

}
