package com.emb.ci.web;

import com.emb.ci.ejb.CiEJB;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author bu
 */
@Named(value = "ci")
@ViewScoped
public class Ci implements Serializable {

    @Inject
    private CiEJB eJB;
    private String greeting;

    @PostConstruct
    public void init() {
        greeting = eJB.getGreeting();
    }

    /**
     * Creates a new instance of Ci
     */
    public Ci() {
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

}
