package com.emb.ci.ejb;

import javax.ejb.Stateless;

/**
 *
 * @author bu
 */
@Stateless
public class CiEJB {

    public String getGreeting() {
        return "Hello CI Testing";
    }
    
        public String doTest() {
        return "Hello";
    }
}
